const mix = require('laravel-mix')

// to compare to latest default version from laravel-mix, see:
// node_modules/laravel-mix/setup/webpack.mix.js

mix.setPublicPath('public')

mix.react('src/theme-custom.jsx', 'public/')
mix.sass('src/theme-custom.scss', 'public/').options({
	// https://laravel.com/docs/7.x/mix#url-processing
	processCssUrls: false,
})
mix.copy('src/images', 'public/images')

mix.webpackConfig({
	// https://webpack.js.org/configuration/dev-server
	devServer: {
		inline: true,
		port: 8080,
		// make it publicly available
		// host: '0.0.0.0',
		// disableHostCheck: true,
	},
	watchOptions: {
		ignored: /node_modules/
	},
})
