# rapidmolding2021 wordpress theme customzations

For `altitude-pro`, part of the Genesis Framework  
Built on `@cblackcom/front` template

## Site Update Strategy

* Spin up the virtual machine in the PNDLM GCE account.  Alternatively, per the instructions below it should be possible to get this site running in a PEH Docker setup.
* Update the IP address for `rapidmoldingdev.pndlm.net`
* `npm run watch` to see the changes live on http://rapidmoldingdev.pndlm.net
* `npm run deploy` to deploy them via SSH to production siteground instance (assuming the VM's public key is still plugged in on SG side)
* Note that in 2023 we are having trouble getting SG to display changes even with all caching turned off.  Use developer tools and "Disable Caching" checkbox to ensure true refresh of production site after deploy.

## Export from GCE to Siteground
Updated 2023-01-29  
Keeping copies of below exported files in PNDLM Dropbox:  
PNDLM Project Archive / Rapidmolding

* Tar up `/var/www/html` (wordpress install)
* Find mysql root password in "Custom Metadata" for the GCE compute instance, or look in `~cblack/wordpress-password.txt`
* `mysqldump -p -u root wordpress > rapidmolding-wordpress-20230129.sql`
* in SQL dump search and replace `http://rapidmoldingdev.pndlm.net` with `https://rapidmolding.com`
* Copy all to Siteground server via SFTP
* Create new database via Siteground Site Tools
* Restore database using mysql cli via SSH
* untar wordpress install
* `npm run prod && npm run deploy` to push compiled JS from this repo to the server
* Re-create symlinks per instructions below
* update settings in `wp-config.php`

That should have been it, but Genesis somehow lost some settings on the way over.

* Appearance > Altitude Pro > Customize > Menus > Main Menu > check the "Header Menu" option
* Appearance > Altitude Pro > Customize > Header Image > select the existing uploaded logo PNG
* Publish changes

### Beware of cache

* Then I had to install "SG Optimizer" plugin, activate it, and use its settings page to "Manually Purge Cache"
* Also fiddled with Speed > Caching settings within Siteground Site Tools

## Original Install

```bash
npm i

# compile initial version
npm run production
```

Install and activate `altitude-pro` theme on Wordpress.

Create a symlink from `wp-content/themes/altitude-pro/rm2021` to the `public` directory here.

Then using Wordpress Admin > Appearance > Customize:

* Site Identity
	* Site Title: RapidMolding
	* Tagline: The first and last partner you�ll need.
* Front Page Background Images: clear all
* Colors
	* Set both to `#B4E500`
* Header Image
	* Upload and set to `rmlogo2021@1x.png` (this will be dynamically swapped with SVG by JS)
* Widgets
	* Set content per `widget-examples.html`
* Theme Settings
	* Set footer per `widget-examples.html`

Finally within Theme Settings > Header/Footer Scripts, set Footer Scripts to:

```html
<link rel="stylesheet" type="text/css" href="/wp-content/themes/altitude-pro/rm2021/theme-custom.css" />
<script src="/wp-content/themes/altitude-pro/rm2021/theme-custom.js"></script>
```

## Use it

```bash
# watch for changes and recompile
npm run watch

# compile development version
npm run dev

# compile production version
# (includes cache busting on index.html)
npm run production
```

# XYZ

