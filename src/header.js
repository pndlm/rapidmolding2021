import $ from 'jquery'

export default () => {
	let swapped = false
	$('nav.nav-primary a').each((idx, el) => {
		// swap first occurrence only
		if(swapped) {
			return
		}
		let href = $(el).attr('href') || ''
		if(href.indexOf('#') >= 0) {
			href = href.substring(0, href.indexOf('#'))
		}
		if(href.charAt(0) == '/') {
			href = 'http://example.com' + href
		}
		const pathname = new URL(href).pathname
		// console.debug(href, pathname)
		if(pathname == "/request-for-quote" || pathname == "/request-for-quote/") {
			$(el).replaceWith(`<button class="request-a-quote">REQUEST A QUOTE</button>`)
			swapped = true
		}
	})
	if(!swapped) {
		console.error('[header.js] Expected request-a-quote link in header to swap for button, did not find...')
	}
}
