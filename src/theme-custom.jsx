import React from 'react'
import ReactDOM from 'react-dom'
import $ from 'jquery'
import initHeader from './header'
import initFooter from './footer'
import initButton from './button'
import StackUp from './stack-up/Root'
import initFP2 from './fp2-injustice'

initHeader()
initFooter()

initButton()

// $(function() {
// 	$('.image-section').css('height', 'auto')
// 	setTimeout(() => {
// 		$('.image-section').css('height', 'auto')
// 	}, 2000)
// })

if($('#stack-up').length > 0) {
	ReactDOM.render(<StackUp />, document.getElementById('stack-up'))
}

initFP2()
