import $ from 'jquery'

export default () => {
	// clean up extraneous "p" elements
	$('footer.site-footer .wrap > :not(div)').remove()
}
