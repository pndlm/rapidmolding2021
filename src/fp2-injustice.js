import $ from 'jquery'
import { pub } from './const'

export default () => {
	$('.injustice > div').each((idx, el) => {
		const $el = $(el)
		const icon = $el.data('icon')
		$el.wrapInner('<div />')
		$el.prepend(`<img src="${pub}/images/${icon}-light.svg" alt="${icon}" />`)
	})
}
