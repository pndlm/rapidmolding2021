import $ from 'jquery'

export default () => {
	$('button.request-a-quote').on('click', () => {
		window.location.href = "/request-for-quote"
	})
}
