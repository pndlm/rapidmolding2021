import React from 'react'
import data from './data'
import { pub } from '../const'
import * as format from './format'

const Stats = React.memo(props => {
	const study = props.study
	const lang = props.lang
	
	return (
		<div>
			<img src={pub + '/images/stack-up/' + study.partphoto} />
			{study.vendors.map((vendor, idx) => (
				<div key={idx} className={'vendor' + (vendor.hometeam == true ? ' hometeam' : '')}>
					<table cellSpacing="0" cellPadding="0">
						<thead>
							<tr>
								<th colSpan="2" className="vendor-name">{vendor.name[lang]}</th>
							</tr>
							<tr>
								<th width="50%">Mold Price</th>
								<th width="50%">{data.strings.partprice[lang]}</th>
							</tr>
						</thead>
						<tbody>
							<tr><td colSpan="2">&nbsp;</td></tr>
							{study.breaks.map((qty, i) => (
								<tr key={i}>
									<td>{i === 0 ? format.nre(vendor.nre) : ''}</td>
									<td>
										{format.partPrice(vendor.partpricing[i])}
										&nbsp;@&nbsp;
										{format.partShort(qty)}
									</td>
								</tr>
							))}
						</tbody>
					</table>
				</div>
			))}
		</div>
	)
})

export default Stats
