
const data = {
	strings: {
		title: {
			en: 'See how we stack up to the competition',
			es: 'Vea cómo nos comparamos con la competencia',
		},
		description: {
			en: 'Project Description',
			es: 'Descripción del Proyecto',
		},
		partprice: {
			en: 'Part Price',
			es: 'Precio Pieza',
		},
	},
	studies: [
		{
			title: {
				en: 'Low Complexity Bushing',
				es: 'Buje de baja complejidad',
			},
			partphoto: 'part-bushing.png',
			breaks: [
				100000,
				250000,
				500000,
				1000000,
			],
			vendors: [
				{
					hometeam: true,
					name: {
						en: 'RapidMolding',
						es: 'RapidMolding',
					},
					nre: 11470,
					partpricing: [
						0.1082,
						0.0978,
						0.0934,
						0.0911,
					]
				},
				{
					hometeam: false,
					name: {
						en: 'Chinese Molder',
						es: 'Moldeador de China'
					},
					nre: 6450,
					partpricing: [
						0.1500,
						0.1469,
						0.1461,
						0.1410,
					]
				},
			],
			description: {
				en: [
					"The customer needed 250K parts ASAP and then 100K parts/month thereafter.  The parts were to be made with a very soft TPU (Thermoplastic Urethane).",
					"Our Chinese-based competition quoted a lead-time of 3 weeks, plus sample approval before production; The first shipment of parts was estimated to leave China at 28 days.  Their price included international freight, but not tariffs, duties or taxes.",
					"RapidMolding provided a multi-cavity mold in 10 calendar days and began shipping production the day after.  The mold is guaranteed for life.",
					"The customer's overall cost was lower with RapidMolding starting from the first full order of 250K parts.",
				],
				es: [
					"El cliente necesitaba 250.000 piezas lo antes posible y luego 100.000 piezas / mes a partir de entonces. Las piezas debían fabricarse con un TPU muy suave (uretano termoplástico).",
					"Nuestra competencia con sede en China cotizó un plazo de entrega de 3 semanas, más la aprobación de la muestra antes de la producción; Se estimó que el primer envío de piezas saldría de China a los 28 días. Su precio incluía fletes internacionales, pero no aranceles, aranceles e impuestos.",
					"RapidMolding proporcionó un molde de múltiples cavidades en 10 días calendario y comenzó a enviar la producción al día siguiente. El molde está garantizado de por vida.",
					"El costo total del cliente fue menor con RapidMolds a partir del primer pedido completo de 250.000 piezas.",
				],
			},
		}
	]
}

export default data
