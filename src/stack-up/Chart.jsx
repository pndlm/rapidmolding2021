import React, { useState, useEffect } from 'react'
import { ResponsiveContainer, LineChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Line } from 'recharts'
import { COLOR_HOME, COLOR_AWAY, COLOR_SEPARATOR_BRIGHT, COLOR_SEPARATOR_DULL } from '../const'
import * as format from './format'

const Chart = React.memo(props => {
	const [data, setData] = useState(null)
	const [homeDataKey, setHomeDataKey] = useState(null)

	useEffect(() => {
		// const colors = props.study.vendors.map(
		// 	vendor => vendor.hometeam == true ? COLOR_HOME : COLOR_AWAY
		// )
		// setColors(colors)

		const study = props.study

		const data = [
			{
				qty: 0,
				name: 'Mold + Samples',
			},
			...study.breaks.map(qty => ({
				qty,
				name: `Mold + ${format.partShort(qty)} Parts`,
			}))
		]

		let homeDataKey = null

		study.vendors.map((vendor, vi) => {
			if(vendor.hometeam == true) {
				homeDataKey = 'v' + vi
			}
			data.map((point, pi) => {
				const partprice = (pi == 0) ? 0 : vendor.partpricing[pi - 1]
				point['v' + vi] = (point.qty * partprice) + vendor.nre
			})
		})

		console.debug('[stack-up/Chart.jsx] data', data)
		setData(data)
		setHomeDataKey(homeDataKey)
	}, [props.study])

	const study = props.study
	const lang = props.lang

	return (
		<div className="graph-area">
			{data !== null ? (
				<ResponsiveContainer>
					<LineChart data={data}>
						<CartesianGrid
							stroke={COLOR_SEPARATOR_DULL}
						/>
						<XAxis
							axisLine={{stroke: COLOR_SEPARATOR_BRIGHT}}
							tick={{stroke: COLOR_SEPARATOR_DULL, fontSize: 16, fontWeight: 200}}
							dataKey="qty"
							tickFormatter={format.partShort}
							tickLine={false}
						/>
						<YAxis
							axisLine={{stroke: COLOR_SEPARATOR_BRIGHT}}
							tick={{stroke: COLOR_SEPARATOR_DULL, fontSize: 16, fontWeight: 200}}
							tickFormatter={format.nreShort}
							tickLine={false}
						/>
						<Tooltip
							isAnimationActive={false}
							content={<CustomToolTip homeDataKey={homeDataKey} lang={lang} />}
						/>
						{/* <Legend /> */}
						{study.vendors.map((vendor, vi) => (
							<Line
								key={'v' + vi}
								dataKey={'v' + vi}
								name={vendor.name[lang]}
								stroke={vendor.hometeam ? COLOR_HOME : COLOR_AWAY}
								dot={{
									fill: vendor.hometeam ? COLOR_HOME : COLOR_AWAY,
									r: 8,
								}}
								activeDot={{
									stroke: vendor.hometeam ? COLOR_HOME : COLOR_AWAY,
									strokeWidth: 1,
									fill: 'black',
								}}
								type="monotone"
								strokeDasharray="5 5"
								animationDuration={750}
							/>
						))}
					</LineChart>
				</ResponsiveContainer>
			) : null}
		</div>
	)
})

const CustomToolTip = React.memo(props => {
	const lang = props.lang

	if(!props.active || !props.payload || !props.payload.length) {
		return null
	}

	return (
		<div className="chart-tooltip">
			{props.payload.map((vendor, vi) => (
				<div key={vi} className={vendor.dataKey == props.homeDataKey ? 'hometeam' : ''}>
					<div className="vendor-name">{vendor.name}</div>
					<div className="value">
						{vendor.payload.name} for {format.nre(vendor.value)}
					</div>
					<svg xmlns="http://www.w3.org/2000/svg" width="126.592" height="9.701" viewBox="0 0 126.592 9.701">
						<path d="M32.417,92.063S141.505,80.67,156.7,90.184c2.53,4.382-3.469,5.773-3.469,5.773s-9.923-8.823-120.816.137C28.629,94.612,32.417,92.063,32.417,92.063Z" transform="translate(-30.734 -86.393)" fill="#fff"/>
					</svg>
				</div>
			))}
		</div>
	)
})

export default Chart
