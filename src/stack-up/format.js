
const fmtNre = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD',
	maximumFractionDigits: 0,
})

export const nre = value => fmtNre.format(value)

const fmtPartPrice = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD',
	minimumFractionDigits: 4,
	maximumFractionDigits: 4,
})

export const partPrice = value => fmtPartPrice.format(value)

const short = (value, extraSymbol = '') => {
	// console.debug('short', value, extraSymbol)
	if(value === null || value === undefined) {
		return '--'
	}
	let symbols = (value < 0) ? '-' : '' + extraSymbol
	const absValue = Math.abs(value)
	// console.debug('absValue', absValue)
	if(absValue >= 1000000) {
		return symbols + (parseFloat(absValue) / 1000000).toFixed(0) + 'M'
	} else if(absValue > 0) {
		return symbols + (parseFloat(absValue) / 1000).toFixed(0) + 'K'
	} else {
		return `${symbols}0`
	}
}

export const partShort = value => short(value, '')
export const nreShort = value => short(value, '$')
