import React from 'react'
import data from './data'
import Chart from './Chart'
import Stats from './Stats'

const Root = React.memo(props => {
	const study = data.studies[0]
	const lang = (document.documentElement.lang.startsWith('es')) ? 'es' : 'en';

	return (
		<div className="root">
			<h4>{data.strings.title[lang]}</h4>
			<ul>
				{data.studies.map((study, idx) => {
					<NavItem key={idx} title={study.title[lang]} />
				})}
			</ul>
			<div className="study">
				<div>
					<Chart study={study} lang={lang} />
					<div className="description">
						<div className="data-heading">
							<div>{data.strings.description[lang]}</div>
						</div>
						<div className="description-content">
							{study.description[lang].map((p, i) => (
								<p key={i}>{p}</p>
							))}
						</div>
					</div>
				</div>
				<Stats study={study} lang={lang} />
			</div>
		</div>
	)
})

const NavItem = React.memo(props => {
	return (
		<li>{props.title}</li>
	)
})

export default Root


