
export const pub = '/wp-content/themes/altitude-pro/rm2021'
export const COLOR_HOME = '#B4E500'
export const COLOR_AWAY = '#AFAFAF'
export const COLOR_SEPARATOR_BRIGHT = '#997BC3'
export const COLOR_SEPARATOR_DULL = '#48395E'
